import java.util.Scanner;

public class Exercicio12 {

	/**
	 * Ex 12. Escreva um programa que receba o n�mero do m�s
	 * e mostre o m�s correspondente. Valide m�s inv�lido.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		int mes;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Informe o n�mero do m�s: ");
		mes = leitor.nextInt();
		
		System.out.print("O m�s informado �: ");
		switch (mes) {
		case 1:
			System.out.println("JANEIRO");
			break;
		case 2:
			System.out.println("FEVEREIRO");
			break;
		case 3:
			System.out.println("MAR�O");
			break;
		case 4:
			System.out.println("ABRIL");
			break;
		case 5:
			System.out.println("MAIO");
			break;
		case 6:
			System.out.println("JUNHO");
			break;
		case 7:
			System.out.println("JULHO");
			break;
		case 8:
			System.out.println("AGOSTO");
			break;
		case 9:
			System.out.println("SETEMBRO");
			break;
		case 10:
			System.out.println("OUTUBRO");
			break;
		case 11:
			System.out.println("NOVEMBRO");
			break;
		case 12:
			System.out.println("DEZEMBRO");
			break;
		default:
			System.out.println("INV�LIDO");
			break;
		}
		
		leitor.close();
	}
}