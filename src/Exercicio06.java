import java.util.Scanner;

public class Exercicio06 {

	/**
	 * Ex 6. Ler uma temperatura em graus Celsius e
	 * apresent�-la convertida em graus Fahrenheit. A
	 * f�rmula de convers�o �: F = (9 * C + 160) / 5
	 */
	public static void main(String[] args) {

		double celsius, fahrenheit;
		
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe a temperatura em Celsius: ");
		celsius = leitor.nextDouble();
		
		fahrenheit = (9 * celsius + 160) / 5;
		
		System.out.printf("Temperatura em Fahrenheit: %.0f", fahrenheit);
		
		leitor.close();
	}
}