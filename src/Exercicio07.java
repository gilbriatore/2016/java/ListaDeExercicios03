import java.util.Scanner;

public class Exercicio07 {

	/**
	 * Ex 7. Escreva um programa que leia um n�mero e diga,
	 * atrav�s de uma mensagem, se este n�mero est� no
	 * intervalo entre 100 e 200. Caso o n�mero esteja fora
	 * do intervalo o usu�rio tamb�m dever� ser informado.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		int numero;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe um n�mero: ");
		numero = leitor.nextInt();
		
		if (numero >= 100 && numero <= 200){
			System.out.printf("O n�mero %d est� entre o intervalo!", numero);
		} else {
			System.out.printf("O n�mero %d est� fora do intervalo!", numero);
		}
		leitor.close();
	}
}