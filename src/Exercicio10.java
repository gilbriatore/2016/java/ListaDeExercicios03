import java.util.Scanner;

public class Exercicio10 {

	/**
	 * Ex 10. Escreva um programa que leia um n�mero de 1 a
	 * 5 e escreva-o por extenso. Caso o usu�rio digite um
	 * valor que n�o esteja neste intervalo, exibir a
	 * mensagem: �N�mero inv�lido! �.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		int numero;
		
		System.out.println("Informe um n�mero: ");
		Scanner leitor = new Scanner(System.in);
		numero = leitor.nextInt();
		
		System.out.print("O n�mero digitado �: ");
		switch (numero) {
		case 1:
			System.out.print("UM");
			break;
		case 2:
			System.out.print("DOIS");
			break;
		case 3:
			System.out.print("TR�S");
			break;		
		case 4:
			System.out.print("QUATRO");
			break;
		case 5:
			System.out.print("CINCO");
			break;
		default: 
		    System.out.println("INV�LIDO");
		}
		
		leitor.close();
	}
}