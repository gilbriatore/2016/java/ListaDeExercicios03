import java.util.Scanner;

public class Exercicio17 {

	/**
	 * Ex 17. Elabore um programa que receba tr�s notas de
	 * um aluno os pesos referentes a cada nota e retorne a
	 * sua m�dia ponderada. C�lculo da m�dia ponderada:
	 * media = (nota1 * peso1 + nota2 * peso2 + nota3 *
	 * peso3) / (peso1 + peso2 + peso3)
	 * 
	 * Exemplo: nota1 = 10.0, nota2 = 5.5, nota3 = 8.0,
	 * peso1 = 5, peso2 = 3, peso3 = 2. M�dia: 8.25
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		double media, nota1, peso1, nota2, peso2, nota3, peso3;
		Scanner leitor = new Scanner(System.in);

		System.out.println("Informe a NOTA 1: ");
		nota1 = leitor.nextDouble();

		System.out.println("Informe o PESO 1: ");
		peso1 = leitor.nextDouble();

		System.out.println("Informe a NOTA 2: ");
		nota2 = leitor.nextDouble();

		System.out.println("Informe o PESO 2: ");
		peso2 = leitor.nextDouble();

		System.out.println("Informe a NOTA 3: ");
		nota3 = leitor.nextDouble();

		System.out.println("Informe o PESO 3: ");
		peso3 = leitor.nextDouble();

		media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

		System.out.printf("A m�dia ponderada �: %.2f", media);

		leitor.close();
	}
}