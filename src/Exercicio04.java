import java.util.Scanner;

public class Exercicio04 {
	/**
	 * Ex 4. Escreva um programa que leia dois n�meros e ao
	 * final mostre a soma, subtra��o, multiplica��o e a
	 * divis�o dos n�meros lidos.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		float num1, num2, r1, r2, r3, r4;

		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o primeiro n�mero: ");
		num1 = leitor.nextFloat();

		System.out.println("Digite o segundo n�mero: ");
		num2 = leitor.nextFloat();

		r1 = num1 + num2;
		r2 = num1 - num2;
		r3 = num1 * num2;
		r4 = num1 / num2;

		System.out.printf("Adi��o: %.2f\n", r1);
		System.out.printf("Subtra��o: %.2f\n", r2);
		System.out.printf("Multiplica��o: %.2f\n", r3);
		System.out.printf("Divis�o: %.2f\n", r4);
		leitor.close();
	}
}