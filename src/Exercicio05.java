import java.util.Scanner;

public class Exercicio05 {

	/**
	 * Ex 5. Escreva um programa que leia valores nas
	 * vari�veis A e B, e efetue a troca dos valores de
	 * forma que o valor da vari�vel A passe a ser o valor
	 * da vari�vel B e o valor da vari�vel B passe a ser o
	 * valor da vari�vel A. Apresentar uma mensagem com o
	 * valor original de cada vari�vel e outra com os
	 * valores trocados.
	 */
	public static void main(String[] args) {
		
		int a, b, temp;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe o valor de A: ");
		a = leitor.nextInt();
		System.out.println("Informa o valor de B: ");
		b = leitor.nextInt();
		
		System.out.println("----- Antes da troca -------");
		System.out.printf("O valor de A �: %d\n", a);
		System.out.printf("O valor de B �: %d\n\n", b);
		
		temp = a;
		a = b;
		b = temp;
		
		System.out.println("----- Depois da troca -------");
		System.out.printf("O valor de A �: %d\n", a);
		System.out.printf("O valor de B �: %d\n\n", b);
		
		leitor.close();
	}
}