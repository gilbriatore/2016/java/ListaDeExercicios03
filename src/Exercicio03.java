import java.util.Scanner;

public class Exercicio03 {

	/**
	 * Ex 3. Escreva um programa que leia os valores de dois
	 * n�meros inteiros distintos nas vari�veis A e B e
	 * informe qual deles � o maior. Caso os n�meros sejam
	 * iguais informar ao usu�rio que a sequ�ncia de n�meros
	 * informados � inv�lida.
	 */
	public static void main(String[] args) {

		int a, b;

		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe o n�mero A: ");
		a = leitor.nextInt();

		System.out.println("Informe o n�mero B: ");
		b = leitor.nextInt();

		if (a > b) {
			System.out.println("O n�mero A � maior do que B!");
		} else if (b > a) {
			System.out.println("O m�mero B � maior do que A!");
		} else {
			System.out.println("Os n�meros s�o inv�lidos!");
		}

		leitor.close();
	}
}