import java.util.Scanner;

public class Exercicio20 {

	/**
	 * Ex 20. Elabore um programa que calcule a quantidade
	 * de litros de combust�vel gasta em uma viagem,
	 * utilizando um autom�vel que faz 12km por litro e
	 * considerando que s�o fornecidos o tempo em hora e a
	 * velocidade m�dia da viagem.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		double consumo, tempo, velocidadeMedia, distancia;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Informe o tempo de viagem em horas: ");
		tempo = leitor.nextDouble();
		
		System.out.println("Informe a velocidade m�dia: ");
		velocidadeMedia = leitor.nextDouble();
		
		//velocidade m�dia = dist�ncia(km) / tempo(h)
		distancia = velocidadeMedia * tempo;
		consumo = distancia / 12;
		
		System.out.printf("A dist�ncia percorrida foi de %.2fkm\n", distancia);
		System.out.printf("O consumo em litros � %.2f", consumo);
		
		leitor.close();
	}
}