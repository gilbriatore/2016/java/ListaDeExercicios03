import java.util.Scanner;

public class Exercicio01 {

	/**
	 * Ex 1. Escreva um algoritmo que leia um n�mero
	 * digitado pelo usu�rio e mostre a mensagem �N�mero
	 * maior do que 10!�, caso este n�mero seja maior, ou
	 * �N�mero menor ou igual a 10!�, caso este n�mero seja
	 * menor ou igual.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		int numero;

		System.out.println("Digite um n�mero:");

		// Faz a leitura do valor digitado
		Scanner leitor = new Scanner(System.in);
		String txt = leitor.nextLine();

		// Converte de texto para inteiro
		numero = Integer.parseInt(txt);

		// Verifica se o n�mero � maior, menor ou igual a 10...
		if (numero > 10) {
			// Mostrar mensagem: �N�mero maior do que 10!�
			System.out.println("N�mero maior do que 10!");
		} else {
			// Mostrar mensagem: �N�mero menor ou igual a 10!�
			System.out.println("N�mero menor ou igual a 10!");
		}

		// Fecha o leitor.
		leitor.close();
	}
}