import java.util.Scanner;

public class Exercicio13 {
	/**
	 * Ex 13. Escreva um programa que leia valores REAIS nas
	 * vari�veis A e B e o tipo de operador em outra
	 * vari�vel do tipo CARACTERE. Imprima o resultado da
	 * opera��o de A por B se o operador aritm�tico for
	 * v�lido; caso contr�rio deve ser impresso uma mensagem
	 * de operador n�o definido. Tratar erro de divis�o por
	 * zero.
	 */
	public static void main(String[] args) {
		
		double a, b, resultado;
		char operador;
		
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe o n�mero A: ");
		a = leitor.nextDouble();
		
		System.out.println("Informe o n�mero B: ");
		b = leitor.nextDouble();
		
		//hack: necess�rio para mudan�a no tipo de leitura.
		leitor.nextLine(); 
		
		System.out.println("Informe um operador (+, -, *, /): ");	
		operador = leitor.nextLine().charAt(0);
		
		switch (operador) {
		case '+':
			resultado = a + b;
			System.out.printf("O resultado �: %.2f", resultado);
			break;
		case '-':
			resultado = a - b;
			System.out.printf("O resultado �: %.2f", resultado);
			break;
		case '*':
			resultado = a * b;
			System.out.printf("O resultado �: %.2f", resultado);
			break;	
		case '/':
			if (b <= 0){
				System.out.println("O divisor deve ser inteiro positivo!");
			} else {
				resultado = a / b;
				System.out.printf("O resultado �: %.2f", resultado);
			}
			break;				
		default:
			System.out.println("Operador inv�lido!");
			break;
		}
		leitor.close();
	}
}