import java.util.Scanner;

public class Exercicio08 {

	/**
	 * Ex 8. Escreva um programa que leia um n�mero e mostre
	 * uma mensagem caso este n�mero seja maior ou igual a
	 * 50, outra se ele for menor que 50.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		int numero;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe um n�mero: ");
		numero = leitor.nextInt();
		
		if (numero >= 50){
			System.out.printf("O n�mero %d � maior ou igual a 50!", numero);
		} else {
			System.out.printf("O n�mero %d � menor que 50!", numero);
		}
		leitor.close();
	}
}