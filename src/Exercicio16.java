import java.util.Scanner;

public class Exercicio16 {

	/**
	 * Ex 16. Elabore um programa que receba tr�s notas de
	 * um aluno e retorne a sua m�dia aritm�tica. Exemplo:
	 * nota1 = 10.0, nota2 = 5.5, nota3 = 8.0. M�dia: 7.83
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		double nota1, nota2, nota3, media;
		
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe a primeira nota: ");
		nota1 = leitor.nextDouble();
		
		System.out.println("Informe a segunda nota: ");
		nota2 = leitor.nextDouble();
		
		System.out.println("Informe a terceira nota: ");
		nota3 = leitor.nextDouble();
		
		media = (nota1 + nota2 + nota3) / 3;
		
		System.out.printf("A m�dia aritm�tica �: %.2f", media);
		
		leitor.close();
	}
}