import java.util.Scanner;

public class Exercicio15 {

	/**
	 * Ex 15. Tendo como dados de entrada dois pontos
	 * quaisquer no plano, P1(x1, y1) e P2(x2,y2), calcule e
	 * retorne a dist�ncia entre eles. A f�rmula que efetua
	 * tal c�lculo �: d = raiz ( ( ( x2 - x1 ) ^ 2 ) + ( (
	 * y2 - y1 ) ^ 2 ) ) Exemplo: p1(0, 5), p2(10, 20).
	 * Distancia: 18,03
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		double d, x1, x2, y1, y2;
		
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe a coordenada x':");
		x1 = leitor.nextInt();
		
		System.out.println("Informe a coordenada x'':");
		x2 = leitor.nextInt();
		
		System.out.println("Informe a coordenada y':");
		y1 = leitor.nextInt();
		
		System.out.println("Informe a coordenada y'':");
		y2 = leitor.nextInt();
		
		//Math.sqrt(); -> extra� a raiz quadrada.
		//Math.pow(valor,2); -> faz a eleva��o do valor ao quadrado.
		
		d = Math.sqrt((Math.pow((x2 - x1), 2)) + (Math.pow((y2 -y1), 2)));
		
		System.out.printf("Dist�ncia: %.2f", d); 
		
		leitor.close();
	}
}