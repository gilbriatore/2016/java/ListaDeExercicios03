import java.util.Scanner;

public class Exercicio19 {

	/**
	 * Ex 19. Fa�a um programa que receba o raio e a altura
	 * de um cilindro e retorne o seu volume calculado de
	 * acordo com a seguinte f�rmula: 
	 * volume = 3.14 * raio^2 * altura; 
	 * Exemplo: raio = 10, altura = 15. Volume = 4710
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		double volume, raio, altura;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Informe o raio: ");
		raio = leitor.nextDouble();
	
		System.out.println("Informe a altura: ");
		altura = leitor.nextDouble();
		
		//Math.pow(raio, 2) eleva o raio ao quadrado;
		volume = 3.14 * Math.pow(raio, 2) * altura; 
		
		System.out.printf("O volume �: %.2f", volume);
		
		leitor.close();
	}
}