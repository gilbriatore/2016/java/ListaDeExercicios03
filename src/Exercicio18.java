import java.util.Scanner;

public class Exercicio18 {

	/**
	 * Ex 18. Elabore um programa que receba tr�s notas de
	 * um aluno e retorne a sua m�dia harm�nica. 
	 * F�rmula: 3 / ((1 / nota1) + (1 / nota2) + (1 / nota3))
	 * 
	 * Exemplo: nota1 = 10.0, nota2 = 5.5, nota3 = 8.0. M�dia: 7.37
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		double media, nota1, nota2, nota3;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Informe a primeira nota: ");
		nota1 = leitor.nextDouble();
		
		System.out.println("Informe a segunda nota: ");
		nota2 = leitor.nextDouble();
		
		System.out.println("Informe a terceira nota: ");
		nota3 = leitor.nextDouble();
		
		media = 3 / ((1 / nota1) + (1 / nota2) + (1 / nota3));
		
		System.out.printf("A m�dia harm�nica �: %.2f", media);
		
		leitor.close();
	}
}