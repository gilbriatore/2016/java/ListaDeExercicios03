import java.util.Scanner;

public class Exercicio02 {

	/**
	 * Ex 2. Escreva um programa que leia dois n�meros
	 * digitados pelo usu�rio e exiba o resultado da sua
	 * soma.
	 */
	public static void main(String[] args) {

		// n�meros decimais
		double numero1, numero2, resultado;

		// informa o usu�rio
		System.out.println("Digite o primeiro n�mero");

		// cria o leitor de entradas do usu�rio
		Scanner leitor = new Scanner(System.in);

		// l� o valor digitado no tipo texto.
		String temp = leitor.nextLine();

		// converte o valor de texto para n�mero.
		numero1 = Double.parseDouble(temp);

		// informa o usu�rio
		System.out.println("Digite o segundo n�mero");

		// faz a leitura
		String temp2 = leitor.nextLine();

		// converte o n�mero
		numero2 = Double.parseDouble(temp2);

		resultado = numero1 + numero2;

		System.out.println("O resultado �: " + resultado);

		// Fecha o leitor.
		leitor.close();
	}
}