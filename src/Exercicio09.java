import java.util.Scanner;

public class Exercicio09 {

	/**
	 * Ex 9. Leia dois n�meros nas vari�veis A e B e
	 * identifique se os valores s�o iguais ou diferentes.
	 * Caso eles sejam iguais imprima uma mensagem dizendo
	 * que s�o iguais. Caso sejam diferentes, informe que
	 * s�o diferentes e qual n�mero � o maior.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {

		int a, b;
		
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Informe o n�mero A: ");
		a = leitor.nextInt();
		
		System.out.println("Informe o n�mero B: ");
		b = leitor.nextInt();
		
		if (a == b){
			System.out.println("Os n�meros s�o iguais!");
		} else {
			System.out.print("Os n�mero s�o diferentes e ");
			if (a > b){
				System.out.println("A � maior do que B!");
			} else {
				System.out.println("B � maior do que A!");
			}
		}	
		
		leitor.close();
	}
}