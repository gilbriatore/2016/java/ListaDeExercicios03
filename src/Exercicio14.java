import java.util.Scanner;

public class Exercicio14 {

	/**
	 * Ex 14. A express�o an = a1 + (n � 1) * r � denominada
	 * termo geral da Progress�o Aritm�tica (PA). Nesta
	 * f�rmula, temos que an � o termo de ordem n (n-�simo
	 * termo), r � a raz�o e a1 � o primeiro termo da
	 * Progress�o Aritm�tica. Escreva um algoritmo que
	 * encontre o n-�simo termo de uma progress�o
	 * aritm�tica. Exemplo: a1 = 10, n = 7, r = 3.
	 * Resultado: an = 28
	 */
	public static void main(String[] args) {
		
		int an, a1, n, r;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Informe o primeiro termo: ");
		a1 = leitor.nextInt();
		
		System.out.println("Informe a raz�o: ");
		r = leitor.nextInt();
		
		System.out.println("Informe o n-�simo termo desejado: ");
		n = leitor.nextInt();

		an = a1 + (n - 1) * r;
		
		System.out.printf("Resultado: %d", an);
		
		leitor.close();
	}
}